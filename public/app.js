function getText() {
  return $("#workingArea").val()
}

function setText(text) {
  $("#workingArea").val(text)
}

const separator = "\n"

function getRows() {
  return getText().split(separator)
}

function setRows(rows) {
  setText(rows.join(separator))
}

function process(fn) {
  setText(fn(getText()))
}

function processRows(fn) {
  setRows(fn(getRows()))
}

function processOnClick(btnId, fn) {
  $(`#${btnId}`).click(() => process(fn))
}


function processRowsOnClick(btnId, fn) {
  $(`#${btnId}`).click(() => processRows(fn))
}

// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffle(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

// https://stackoverflow.com/questions/9229645/remove-duplicate-values-from-js-array
function unique(array) {
  return array.filter(function(item, pos) {
    return array.indexOf(item) == pos;
  })
}

$( document ).ready(function() {
    console.log(navigator.clipboard.readText, !navigator.clipboard.readText)
    if(!navigator.clipboard.readText) {
      console.log('disabled')
      $('#fromClipboard').prop('disabled', true)
    }
    console.log( "ready!" );
    processOnClick("toLower", x => x.toLowerCase())
    processOnClick("toUpper", x => x.toUpperCase())
    processRowsOnClick("sort", x => x.sort())
    processRowsOnClick("unique", x => unique(x))
    processRowsOnClick("reverse", x => x.reverse())
    processRowsOnClick("shuffle", x => shuffle(x))
    processOnClick("generateUuidV4", () => uuid.v4())
    $("#toClipboard").click(() => {
      navigator.clipboard.writeText(getText())
    })
    $("#fromClipboard").click(() => {
      navigator.clipboard.readText()
      .then(text => {
        setText(text)
      })
      .catch(err => {
        alert(err)
      });
    })
});
